import uuid
import time
import datetime
import requests
import jsonpath
import os
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_v1_5 as pkcs
import base64
import webbrowser
import random
import math
import json

def get_he(shopId, choice, i):

    if choice == 'meal':
        url = '/fulfill/weborder/queryInProcessOrders/?method=OrderWebService.queryInProcessOrders'
    elif choice == 'comm':
        url = '/ugc/invoke/?method=ShopRatingService.getRateResult'
    else:
        print('暂未实现此功能\n')
        return
    
    ze, index = 0,0
    while index < len(url):
        ze =(31 * ze) & 0xFFFFFFFF
        if ze & 0x80000000:
            ze -= 0x100000000
        ze += ord(url[index])
        index += 4

    he = [242, 64, 91, 27] + [ 255 & (ze>>8), 255 & ze, 216, 237, 31] + timestamp_list + [1, i, 7, 0]
    #he = [242, 64, 91, 27] + [191, 102, 216, 237, 31] + timestamp_list + [1, random.randint(1,999), 7, 0]  # 出餐，1和7之间是 ba[70]
    
    # url = 'https://napos-order-pc.faas.ele.me/app/processing/meal'
    # url = 'https://melody-comment.faas.ele.me/'
    if choice == 'meal':
        he = he + [54,104,116,116,112,115,58,47,47,110,97,112,111,115,45,111,114,100,101,114,45,112,99,46,102,97,97,115,46,101,108,101,46,109,101,47,97,112,112,47,112,114,111,99,101,115,115,105,110,103,47,109,101,97,108]
    else:
        he = he + [35,104,116,116,112,115,58,47,47,109,101,108,111,100,121,45,99,111,109,109,101,110,116,46,102,97,97,115,46,101,108,101,46,109,101,47]

    he = he + [8, 78, 101, 116, 115, 99, 97, 112, 101] # Netscape

    he = he + [1,0,0,0,0,0,0,4,0,0,0]

    s = round(random.random() * 4294967296)
    he = he + [255 & (s >> 24), 255 & (s >> 16), 255 & (s >> 8), 255 & s]

    he = he + [0,144,243,33,123, 0,0,0,0,211,174,127,117]
    
    y = round(time.time())
    he = he + [0, 255 & (y >> 24), 255 & (y >> 16), 255 & (y >> 8), 255 & y]
    
    return he

    """
      K = [
        242,64,91,27,  191,102,216,237,31,  1,145,251,150,209,171,   1,47,7,0,
        54,104,116,116,112,115,58,47,47,110,97,112,111,115,45,111,114,100,101,114,45,112,99,46,102,97,97,115,46,101,108,101,46,109,101,47,97,112,112,47,112,114,111,99,101,115,115,105,110,103,47,109,101,97,108,
        8,78,101,116,115,99,97,112,101,
        1,0,0,0,0,0,0,4,0,0,0,      139,244,70,19,      0,144,243,33,123,0,0,0,0,211,174,127,117,
        0,102,232,98,97]
    """


def trans_he_to_Ve(he):

    def div_0(part_list):
        for index in range(len(part_list)):
            item = part_list[index]
            ja = 255 & ( item - 3)
            ja = ( ja >> 5 ) + ( ja << 3 )
            result = ( 255 & ja ) ^ 128
            K.append(result)
 
    def div_1(part_list):
        for index in range(len(part_list)):
            item = part_list[index]
            ja = 255 & ( item - 7 )
            ja = ( ja >> 5 ) + ( ja << 3 )
            result = ( 255 & ja ) ^ 128
            K.append(result)
            
    def div_2(part_list):
        for index in range(len(part_list)):
            item = part_list[index]
            Ba = ( item >> 4 ) + ( item << 4 ) + 19506
            result = (255 & Ba) ^ 128
            K.append(result)
                
    def div_3(part_list):
        for index in range(len(part_list)):
            item = part_list[index]
            k = 74 if index == 0 else 73 
            item = item ^ k
            result = (255 & item) ^ 128
            K.append(result)
            
    K, q = [], 0
    while  2 * q < len(he):
        part_list = he[2 * q : 2 * q+2]
        G = q % 4  #G = ( q%4 + 128) % 4
        if G == 0:
            result = div_0(part_list)
        elif G == 1:
            result = div_1(part_list)
        elif G == 2:
            result = div_2(part_list) 
        elif G == 3:
            result = div_3(part_list)
        q += 1

    w, je = 0, 0
    for item in K:
        je = 255 & ( je + (item & 98) )
    return [len(K)+1, je] + K


def get_Le():

    K = []
    je = math.floor( math.random() * math.pow(2,32) )
    while True:
        ge, je = 127 & je, je >> 7 
        if je != 0:
            ge = 128 | ge
            K.append(ge)
        else:
            K.append(ge)
            break

    K = K + [128, 12] + timestamp_list
          
    time_delta = random.randint(160,1000)
    oe = time_delta % 128
    Ee = (time_delta - oe) / 128
    K = K + [oe+128, 127 & Ee, 0, 0]
    """  
    # 长10的数组_ 得到  re = 1466,       [re>>8 & 255, 255 & re] 得到 【5， 186】
    K = K + [5,186]
    K = K + [ ba[39] ^ ba[49], ba[16] ]   # [0, 1] ?

    _ = ba[0] ^ ba[122]   # ba[0]是变数
    Z = _ % 128
    K = K + [Z + 128,  ( _ - Z)/128 & 127,  255]
        
    K = K +  [ba[94] ^ ba[120], 0]  #  ba[94]是累增的， ba[120]=1,  K[25] k[26]

    K [27] = _ - ba[124]   #  减去的老ba[124]  新ba[124] = _

    k[28] = ba[94] ^ ba[120] - ba [83]   # 减去的老ba[83]  新ba[83] =ba[94] ^ ba[120]

    k[29] = 255 & ( 100 * ba[29] / (ba[94] ^ ba[120]) )

    k[30] = 0

    ba[84] ^ ba[129]
    """


# 处理算法是对的， Le 生成方式不一样
def from_Le_to_K():

    # Le = '253,176,177,250,11, 128,12,     1,145,251,122,183,52,   239,165,11,   0,0,5,186,0,1,24,255,1,0,24,      1,0,100,0,false,254,0,0,0,0'
    #       253,176,177,250,11, 128,12,     1,145,251,122,183,52,   145,223,105,  0,0,5,186,0,1,182,1,255,3,0,87,   1,0,100,0,false,254,0,0,0,0
    #       195,136,184,241,8,  128,12,     1,145,251,150,209,171,  251,239,1,    0,0,5,186,1,1,154,2,255,4,0,146,1,1,0,100,0,false,254,0,0,0,0,'

    je = math.floor( random.random() * math.pow(2,32) )
    K = []
    while True:
        ge, je = 127 & je, je >> 7 
        if je != 0:
            ge = 128 | ge
            K.append(ge)
        else:
            K.append(ge)
            break

    Le = K + [128, 12] + timestamp_list + [251,239,1,  0,0,5,186,   1,1,154,2,255,4,0,146,1,  1,0,100,0,0,254,0,0,0,0]

    def div_0(part_list):
        D = 27396
        for index in range(len(part_list)):
            Z = ( part_list[index] ^ D )
            D = ( D * index ) % 256 + 3952
            item = 255 & Z ^ he_1
            K.append(item)

    def div_1(part_list):
        for index in range(len(part_list)):
            H = part_list[index] ^ ord( "PmCnRfjs"[index] )
            item = 255 & H ^ he_1
            K.append(item)

    def div_2(part_list):
        D = 69
        for index in range(len(part_list)):
            D = (D + 1) % 4
            Z = ord( "jm1d"[D] )
            item = part_list[index] ^ Z & 255 ^ he_1 
            K.append(item)
        
    def div_3(part_list):
        Q = 137
        for index in range(len(part_list)):
            me = part_list[index] ^ Q & 255
            item = me ^ he_1
            Q = me
            K.append(item)

    K, da = [], 0
    while  2 * da < len(Le):
        je = ( ( da % 4 ) + he_1 ) % 4
        part_list = Le[2 * da : 2 * da + 2]
        if je == 0:
            div_0(part_list)
        elif je == 1:
            div_1(part_list)
        elif je == 2:
            div_2(part_list)
        elif je == 3:
            div_3(part_list)
        da += 1
        
    x = 0
    for item in K:
        x = 255 & ((233 & item) + x)
    K = [len(K)+1, x] + K
    return K


# 处理算法是对的 
def get_pa():

    def div_0(part_list):
        Re =188
        for index in range(len(part_list)):
            Re = (Re + 1) % 4
            ae = ord("Gto2"[Re])
            item = part_list[index] ^ ae & 255 ^ he_1
            Ie.append(item)

    def div_1(part_list):
        va = 244
        for index in [0,1]:
            Re = part_list[index] ^ va
            je = Re ^ he_1
            Ie.append(je)
            va = Re

    def div_2(part_list):
        pe, va = 155, 256
        for index in range(len(part_list)):
            je = (part_list[index] + 154 ) % va
            ae = je ^ he_1
            Ie.append(ae)
        
    def div_3(part_list):
        for index in [0,1]:
            result = part_list[index] ^ ord( "QvtkfsL"[index] ) ^ he_1
            Ie.append(result)

    Ie, Me = [], 0
    pa = [251,3,88,102,254,26,15,255,0,0,0,0,0,0,43,44,190,203,   141,174,125,130,   0,0,3,96,0,0,6,0,0,0,0,0,0,0,0,0,0,0,3,56,0,0,6,0]
    while  2 * Me < len(pa):
        De = ( (Me % 4) + he_1) % 4
        part_list = pa[2 * Me : 2 * Me+2]
        if De == 0:
            div_0(part_list)
        elif De == 1:
            div_1(part_list)
        elif De == 2:
            div_2(part_list)
        elif De == 3:
            div_3(part_list)
        Me += 1

    D = 0
    for item in Ie:
        D = 255 & ( D + (191 & item) )
    pa = [len(Ie) +1, D] + Ie
    return pa


def get_bx_et(shopId, choice, i):
  
    global timestamp_list, he_1
    timestamp = round(time.time() * 1000)
    we = math.floor(timestamp / 4294967296)
    Xe = timestamp - (we * 4294967296 )
    timestamp_list = [255 & (we >> 8), 255 & we, 255 & (Xe >> 24), 255 & (Xe >> 16), 255 & (Xe >> 8), 255 & Xe]
    he_1 = 255 & Xe
        
    he = get_he(shopId, choice, i)
    ve = trans_he_to_Ve(he)
    ge = ve + from_Le_to_K() + get_pa()

    ie = 0
    for item in ge:
        N = ie + (149 & item)
        ie = 255 & N
    Ze = [ie, he_1] + ge

    pe = "16s_O9tTGMigNwZaCBIQdpKLhHn3FeEUfXjbAvxYcDm0V2q45WS7RJ-8lkouPyrz."
    pa, J = [], 0

    while J < len(Ze):
        try:
            k2 = Ze[J+2]
        except:
            k2 = 64
        try:
            k1 = Ze[J+1]
        except:
            k1 = 64     
        Le = (Ze[J] << 16) | (k1 << 8)  | k2
        de = Le >> 18
        Ie = 63 & (Le >> 12)
        se = 63 & (Le >> 6)
        Le = 63 & Le
        pa = pa + [ pe[de], pe[Ie], pe[se], pe[Le] ]
        J += 3
    return "e"+ "".join(pa)


def get_ele_shop_dic(chainId, ksid):
    
    visitId = str(uuid.uuid4()).upper().replace("-", "") + "|" + str( round( time.time() * 1000 ) )
    payload = {
        'service':'shop',   'method':'getRestaurantTree',   
        'params':{'restaurantIds': [chainId]},
        'id':visitId,   
        'metas':{'appVersion': '1.0.0', 'appName': 'melody', 'ksid': ksid, 'shopId': chainId},
        'ncp':'2.0.0'
    }
    headers = {
        "accept": "application/json, text/plain, */*",
        "accept-language": "zh-CN,zh;q=0.9",
        "authority": "app-api.shop.ele.me",
        "content-type": "application/json;charset=UTF-8",
        "origin": "https://melody.shop.ele.me",
        "referer": "https://melody.shop.ele.me/",
        "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/127.0.0.0 Safari/537.36",
        "x-eleme-requestid": visitId,
        "x-shard": f"shopid={chainId}"
    }
    res = requests.post("https://app-api.shop.ele.me/shop/invoke?method=shop.getRestaurantTree", json=payload, headers=headers)
    data = res.json()
    name_list = jsonpath.jsonpath(data, "$.result.0.childList.*.name")
    if name_list == False:
        print(f'饿了么商家平台疑似未登录, 正在尝试刷新cookie：{data}')
        return None, -1
    name_list = [name.replace('（','(').replace('）',')').replace('•','·') for name in name_list]
    run_list = jsonpath.jsonpath(data, "$.result.0.childList.*.busyLevel")
    id_list = jsonpath.jsonpath(data, "$.result.0.childList.*.id")
    closed_shop_list = []
    ele_shop_dic = dict(zip(id_list, name_list))
    #ele_shop_dic = dict(sorted(ele_shop_dic.items(), key=lambda x:x[1]))
    shop_length = len(id_list)
    for index in range(shop_length):
        if run_list[index] == 'INVALID':
            del ele_shop_dic[id_list[index]]
            continue
        if run_list[index] in ['SUSPEND_BUSINESS','CLOSE']:
            closed_shop_list.append(name_list[index])
            del ele_shop_dic[id_list[index]]
    return ele_shop_dic, closed_shop_list


def get_mt_shop_dic(cookie, acctId, mode = None):

    headers = {
        "Accept": "application/json, text/plain, */*",
        "Accept-Language": "zh-CN,zh;q=0.9",
        "Connection": "keep-alive",
        "Content-Type": "application/x-www-form-urlencoded",
        "Cookie": cookie,
        "Origin": "https://e.waimai.meituan.com",
        "Referer": "https://e.waimai.meituan.com/",
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/127.0.0.0 Safari/537.36"
        }
    res = requests.post('https://e.waimai.meituan.com/api/poi/poiList?ignoreSetRouterProxy=true', headers=headers)
    data = res.json()
    name_list = jsonpath.jsonpath(data, '$.data.*.poiName')
    if name_list == False:
        raise Exception(f'\n 美团外卖登录异常, 请刷新cookie：{data}')
        input()
        return
    name_list.pop(0)
    name_list = [name.replace('（','(').replace('）',')').replace('•','·') for name in name_list]
    shopId_list = jsonpath.jsonpath(data, '$.data.*.id')
    shopId_list.pop(0)
    shopId_list = [str(shopId) for shopId in shopId_list]
    status_list = jsonpath.jsonpath(data, '$.data.*.businessDesc')
    status_list.pop(0)

    mt_shop_dic = dict(zip(shopId_list, name_list))
    #mt_shop_dic = dict(sorted(mt_shop_dic.items(), key=lambda x:x[1]))
    closed_mt_shop_list = []
    for index in range(len(status_list)):
        if status_list[index] in ['已打烊']:
            closed_mt_shop_list.append(name_list[index])
        if mode == 'meal':
            if status_list[index] != '营业中':
                del mt_shop_dic[shopId_list[index]] 
        elif status_list[index] not in ['已打烊', '仅接受预订', '营业中']:
            del mt_shop_dic[shopId_list[index]]
            
    current_shop_length = len(mt_shop_dic)
    authorized_shop_length, _ = isValid(acctId)
    if authorized_shop_length == 0: # 权限判断
        return {'notice' : '店铺未获授权或授权已过期'}, len(mt_shop_dic)
    elif current_shop_length > authorized_shop_length:
        return {'notice' : f'授权门店{authorized_shop_length}家, 当前{current_shop_length}家'}, len(mt_shop_dic)
    return mt_shop_dic, closed_mt_shop_list


def isValid(acctId):
    
    if os.path.exists(r'.\templete\key') == False:
        raise Exception(' key文件丢失，请联系作者')
    timestamp = time.time()
    pk = b'LS0tLS1CRUdJTiBSU0EgUFJJVkFURSBLRVktLS0tLQpNSUlFb2dJQkFBS0NBUUVBMUx3THhTNU9FcGYvUklNaWVZYngyM0gyZFlxM3pUTkk1ZzJkb0tiUkppQ0E5QTlFClZjZ2wy'
    pk += b'TURTZmVGSWVZU0lBUVhya2ZFUHMyMU9zUHptc1FzdmU2Um9GaEFBMDZ5Skh4QzRFWDVVdXowVysvRWkKR2xSaDJWYVhWanh0bUJkQW1qTHhBbisrVWFHbWJjeU9QTnhBaEc'
    pk += b'3MmptQWFXWStndXdLd0I3SmNFRDhETzB4egpXT0lTUHNUQ3dRZzZ5aVpHcHdsMHFZa2k3dGE0SGRJSm90RGIwY0RvNEp3N2hlcklpMlQxQkwrRXpDUVFVclFnCjlYK0pjcEl3cXVt'
    pk += b'cE5TdTRUcDZZekVYTnFualpwZFlDaEFsZGNhVzVqSFI4SjJ0cm9kcUtYcStNL09pSUJPR1IKbnN0WXF4TXl0Z2VxalZIZjRUNDlEYktrVkF4SzA0UVBNai9INlFJREFRQUJBb0lCQUFkcHBvallS'
    pk += b'bGJxUmpZeApVaFVWUFUzNTNINUh4MFFPbEl5VW5xQ2JrM2ZlUTlRTlNLS3JFdHVJeTRUZ29kMS9IMXlXdzlpOTR6L0xzQ1ZsCmFKWDBkM0w1M1hrZ1ZXRUsxdXZXMGEwbFgyYzN'
    pk += b'WNXJQa2hKRURMVk5xY0FTNHJnOUhpNkpWaUhSeVBSUCsweEIKNTVacUQ5TDFpV0NZYTI2aHZUWnEvMjZET2VkRWRKRkg5eHZ2SGdkZlQ4d3BXVUwwK0h5RHVxZmJYWW5CYXV'
    pk += b'zVgpVVmxSOFhhVDJJeXdSMkxvWFVoWVFyUmpKUnhqeDMvS1hpeklVMG9BYXNIZFNVeVIwNkdoTmQyZVJnTHIzSmswCmFNZURqYSs3RktZbUtaS1hvNklNSjlkUGcySng0dEpJZiswU'
    pk += b'm92Qmc2d2taTy9TN1kxSmhJUFo5ZFpJcnJLdEcKV0tDa05hRUNnWUVBNk5Fd2JlRWVlT1p2cUhUcXB1eVkwVitTeW9CTkpUZkxnVzBGdEh5Sy8rVWl3T1lONWJaWgpFVWVGR2RqK1h'
    pk += b'KbmZxT1pQSlUza0JUSm5BQWgyRlZYK3dUdS9aaDNXR0piM1lTeGFsNENsZ0Zqc3hUV0VmMzVKCjZQL0RFbFJLUUdQcVFkcWVmc3A3VzZuVnVuekFBTmwyaU9CL0pHRjNWenlJd0gr'
    pk += b'RFRxY2VPeUVDZ1lFQTZlcnMKNzhYMVN1Y0s4U0YxU3RvN3lLZ3lwRFhDYWVpS3dMUGg3eGZBT3EwMEJkNGhGVHVmTS9FUllzakk3ekE0T2EyRApTKzVtY1N1c1FzNUFLUXBPbGx5NU'
    pk += b'hKYWdnMjljSTJVMFp5UHJKaDBNN3NQSU5UQVVHbENkNjVnUlNOak9KdnRNCkQzSFBOYTlaN04xUHlNSTh4MlhQK0xJODVFb2RWYnJRTGtCSys4a0NnWUJRbm8rc25QWFdXOFZ5a'
    pk += b'WZudmo2SmoKYmpzQWtHa21hQ202U2F4ZUVYcDRuam5vbmtGVnVFMGpaS2E4aXAyYUoxYUJ3QzdMOWlydGVjU2RDRkVMbGxtLwpSQmRGSHpQU0ZBVFp5MmdiYWxybEZPWDR'
    pk += b'iRTNUY0VIYnd5VWpwYmlJajVEQStSbEZRb3h3ODJDbFVTbVRvYm9nCkw4MFhKZ0VUWGt6a2k4TkdheXhXd1FLQmdGT0hDZmhNcmR5RFNBcjJzWlY2L0NJWXZZeVpqY1NYdndRMH'
    pk += b'Q1YlMKMmxlWmhad2F4NkYzV3RBSFl2ZnRUcUxxaXdrOWhrYlU2RXU5RnBQOFpqakJiM0tOeGFRSnZXTnhubXNkTm1zbQpEay9ieThSdXNNMGRUL0JkblNhRmRKYVlwSFNUQTRQNF'
    pk += b'ZjK0x3QWF1dUZDdDIxb3dadVVkY0ZUc2lqRVV0a1h0CnUxalJBb0dBVVB1VTBIdzM3UDcyMFA0NEdYSW14c0drU2UvdE41L1UxVVVlcnV5SDJLWlpZbUFqZFFpYUxmRzQKRk1CcGhpOF'
    pk += b'F6a1I0aWNKNzRLcXJtWkhJSGpxT3NtQlVwd3FwM1BGNHFRY3JpcmVYT2FuYk1WOE1xdUFGNzRjeApXM24xY3dFN2hMNEtVblF5R2I0OHZkVjhsZEtwbzFXa0NTRE8zRUxVM2x6dmcxT'
    pk += b'25yWEU9Ci0tLS0tRU5EIFJTQSBQUklWQVRFIEtFWS0tLS0t'
    pk = base64.b64decode(pk).decode()
    cipher = pkcs.new(RSA.importKey(pk))
    
    # 获取授权店铺数、是否有推送权限
    with open(r'.\templete\key','rb') as f:
        encrypted_msg = f.read()
    data_dic = eval(cipher.decrypt(encrypted_msg, 0).decode())
    authorized_acctId = data_dic.get('acctId')
    length, push = 0, 0
    
    if timestamp < data_dic.get('timestamp'):
        if acctId in authorized_acctId.split(','):
            length, push = data_dic.get('length'), data_dic.get('push')
    
    return length, push
